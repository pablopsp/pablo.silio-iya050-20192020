var path = require("path");
const express = require("express");
const { ApolloServer } = require("apollo-server-express");

const typeDefs = require("./backend/schema");
const fetchAllNews = require("./backend/querys").fetchAllNews;
const fetchSingleNews = require("./backend/querys").fetchSingleNews;
const insertSingleNews = require("./backend/querys").insertSingleNews;

const resolvers = {
  Query: {
    all_news: () => fetchAllNews(),
    single_news: (parent, args) => {
      const { id } = args;
      return fetchSingleNews({
        id,
      });
    },
  },
  Mutation: {
    create_news: (parent, args) => {
      const news = args.input;
      return insertSingleNews(news);
    },
  }
};

const app = express();
app.use(express.static("public"));
app.get("/", function (req, res) {
  res.sendFile(path.join(__dirname + "/index.html"));
});


const server = new ApolloServer({
  typeDefs,
  resolvers,
});
server.applyMiddleware({ app });


app.listen(
  {
    port: 4000,
  },
  () =>
    console.log(`Server ready at http://localhost:4000${server.graphqlPath}`)
);

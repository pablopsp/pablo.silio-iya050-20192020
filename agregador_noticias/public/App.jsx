const { React, Navbar, Allnews, Infonew, Createnew } = window;
const { BrowserRouter: Router, Switch, Route } = window.ReactRouterDOM;

const App = () => (
    <Router>
      <Switch>
        <Route exact path="/Readnew/:id" component={props => <React.Fragment><Navbar {...props} /><Infonew {...props} /></React.Fragment>} />
        <Route exact path="/Createnew" component={props => <React.Fragment><Navbar {...props} /><Createnew {...props} /></React.Fragment>} />
        <Route exact path="/" component={props => <React.Fragment><Navbar {...props} /><Allnews {...props} /></React.Fragment>} />
      </Switch>
    </Router>
);

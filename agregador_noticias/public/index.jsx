const { React, ReactDOM, App } = window;
const { BrowserRouter } = window.ReactRouterDOM;

const News = () => {
  localStorage.removeItem('arraystate');
  return (
    <BrowserRouter>
      <App />
    </BrowserRouter>
  );
};

ReactDOM.render(<News />, document.getElementById("root"));

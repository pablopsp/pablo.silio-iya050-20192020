const { React } = window;

class Createnew extends React.Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      escritor: "",
      intro: "",
      contenido: "",
      fecha: "",
      imagen: "",
    };
  }

  handleChange(evt) {
    this.setState({
      [evt.target.name]: evt.target.value,
    });
  }

  canBeSubmitted() {
    const { escritor, intro, contenido, fecha, imagen } = this.state;
    return (
      escritor.length > 0 &&
      intro.length > 0 &&
      contenido.length > 0 &&
      fecha.length > 0 &&
      imagen.length > 0
    );
  }

  handleSubmit(event) {
    const { escritor, intro, contenido, fecha, imagen } = this.state;
    event.preventDefault();

    const query = `mutation{create_news(input:{escritor: "${escritor}",
      intro: "${intro}",
      contenido: "${contenido}",
      fecha: "${fecha}",
      imagen: "${imagen}"})
      {id,escritor,intro,contenido,fecha,imagen}}`

    fetch('http://localhost:4000/graphql?query=' + query, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        query: query
      })
    })
      .then(res => res.json())
      .then(res => {
        console.log(res);
        this.props.history.push(`/`);
      })
  }

  componentDidMount() {
    this.setState({
      fecha: document.querySelector("#start").value,
    });
  }

  render() {
    const submitReady = this.canBeSubmitted();
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <div className="containercreate">
            <div className="row">
              <div className="col-25">
                <label htmlFor="fname"> Nombre del Autor </label>
              </div>
              <div className="col-75">
                <input
                  type="text"
                  id="fname"
                  name="escritor"
                  placeholder="Nombre del autor de la noticia.."
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-25">
                <label htmlFor="lname"> Introducción </label>
              </div>
              <div className="col-75">
                <input
                  type="text"
                  id="intro"
                  name="intro"
                  placeholder="Introducción de la noticia.."
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-25">
                <label htmlFor="subject"> Contenido </label>
              </div>
              <div className="col-75">
                <textarea
                  id="contenido"
                  name="contenido"
                  placeholder="Contenido de la noticia.."
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-25">
                <label htmlFor="start"> Fecha de la noticia </label>{" "}
              </div>
              <div className="col-75">
                <input
                  type="date"
                  id="start"
                  name="fecha"
                  min="2000-01-01"
                  max="2021-01-10"
                  style={{
                    marginTop: "8px",
                  }}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-25">
                <label htmlFor="subject"> Imagen Noticia </label>
              </div>
              <div className="col-75">
                <input
                  type="text"
                  id="imagen"
                  name="imagen"
                  placeholder="Link a la imagen"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="row">
              <center>
                <input
                  className="button"
                  disabled={!submitReady}
                  type="submit"
                  value="Submit"
                ></input>
              </center>
              <button
                className="button"
                onClick={() => this.props.history.push(`/`)}
              >
                Regresar
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

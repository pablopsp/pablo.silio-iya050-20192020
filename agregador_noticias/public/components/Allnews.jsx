const { React } = window;

class Allnews extends React.Component {
  constructor(props) {
    super(props);
    this.Allstate = new Array();
    this.Iniciador();
    this.state = {
      data: []
    };
  }
  Iniciador() {
    const query = `{all_news{id,escritor,intro,contenido,fecha,imagen}}`
    fetch('http://localhost:4000/graphql?query=' + query, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })
      .then(res => res.json())
      .then(json => {
        this.setState({ data: json.data.all_news })
        this.state.data.map((noticia) => {
          this.Localstorage(noticia.id, false, "Tomar");
        });
      });
  }

  Localstorage(newsid, newsread, option) {
    let arraystate = JSON.parse(localStorage.getItem("arraystate"));
    if (arraystate != null) {
      if (arraystate[newsid] != null) {
        if (option == "Tomar") {
          this.Allstate[newsid] = arraystate[newsid];
        } else {
          arraystate[newsid] = newsread;
          localStorage.setItem("arraystate", JSON.stringify(arraystate));
        }
      } else {
        arraystate[newsid] = false;
        localStorage.setItem("arraystate", JSON.stringify(arraystate));
      }
    } else {
      let tempArraystate = new Array();
      tempArraystate[newsid] = false;
      localStorage.setItem("arraystate", JSON.stringify(tempArraystate));
    }
  }

  Savestate(newsid) {
    this.Localstorage(newsid, true, "leido");
    this.props.history.push(`/Readnew/${newsid}`);
  }

  render() {
    const newsarray = this.Allstate;
    return (
      <div>
        {this.state.data !== null ? this.state.data.map((noticia) => {
          return (
            <div key={noticia.id} className="card">
              <img
                src={noticia.imagen}
                alt="Foto de portada"
                className="imgnew"
              />
              <div className="container">
                <center>
                  <h1>
                    <b>{noticia.intro}</b>
                  </h1>
                </center>
                <center>
                  <h2> {"Escrito por " + noticia.escritor} </h2>
                </center>
                <center>
                  <h3>{noticia.fecha}</h3>
                </center>
                <center>
                  <button
                    className="button"
                    style={{
                      backgroundColor: newsarray[noticia.id] ? "Blue" : "green",
                    }}
                    onClick={() => this.Savestate(noticia.id)}
                  >
                    {newsarray[noticia.id] ? "Leer otra vez" : "Leer"}
                  </button>
                </center>
              </div>
            </div>
          );
        }) : this.Iniciador()}
      </div>
    );
  }
}

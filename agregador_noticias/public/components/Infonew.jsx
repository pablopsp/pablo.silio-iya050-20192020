const { React } = window;

class Infonew extends React.Component {
  constructor(props) {
    super(props);
    this.state = { info: null };
  }

  componentDidMount() {
    //fetch a la url de graphql con la query que me da la noticia que tenga el id en especifico
    const query = `{single_news(id:"${this.props.match.params.id}"){escritor,intro,contenido,fecha,imagen}}`
    fetch('http://localhost:4000/graphql?query=' + query, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })
      .then(res => res.json())
      .then(json => {
        this.setState({ info: json.data.single_news })
      });
  }

  render() {
    const note = this.state.info;
    if (note === null) {
      return <div>LOADING</div>;
    }

    return (
      <div className="card">
        <img src={note.imagen} alt="Foto de portada" className="imgnew" />
        <div className="container">
          <center>
            <h1>
              <b>{note.intro}</b>
            </h1>
          </center>
          <center>
            <h2> {"Escrito por " + note.escritor} </h2>
          </center>
          <center>
            <h3>{note.fecha}</h3>{" "}
          </center>
          <p>{note.contenido}</p>
          <button
            className="button"
            onClick={() => this.props.history.push(`/`)}
          >
            Regresar
          </button>
        </div>
      </div>
    );
  }
}

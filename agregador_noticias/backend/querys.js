const fetch = require("node-fetch");
const uuid = require("uuid/v4");

const fetchAllNews = () => {
  return fetch(
    "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development/pairs",
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "x-application-id": "pablo.silio",
      },
    }
  )
    .then((res) => res.json())
    .then((json) => {
      return json.map(x => {
        return JSON.parse(x.value)
      });
    });
};

const fetchSingleNews = ({ id }) => {
  return fetch(
    `https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development/pairs/${id}`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "x-application-id": "pablo.silio",
      },
    }
  ).then((res) => res.json())
    .then((json) => {
      return JSON.parse(json.value)
    });
};

const insertSingleNews = (noticia) => {
  const id = uuid()
  const idJson = { "id": id };
  const body = JSON.stringify(Object.assign(idJson, noticia))
  return fetch(
    `https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development/pairs/${id}`,
    {
      method: "PUT",
      body: body,
      headers: {
        "Content-Type": "application/json",
        "x-application-id": "pablo.silio",
      },
    }
  )
    .then((res) => res.json())
    .catch((error) => console.error("Error: ", error))
    .then((response) => {
      console.log("Success: ", response)
      return JSON.parse(body);
    });
};

module.exports.fetchAllNews = fetchAllNews;
module.exports.fetchSingleNews = fetchSingleNews;
module.exports.insertSingleNews = insertSingleNews;

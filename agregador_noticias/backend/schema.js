const {
  gql
} = require('apollo-server');

const typeDefs = gql `
  type News {
    id: String
    escritor: String
    intro: String
    contenido: String
    fecha: String
    imagen: String
  }

  input NewInput{
    escritor: String
    intro: String
    contenido: String
    fecha: String
    imagen: String
  }

  type Query {
    all_news: [News]
    single_news(id: ID!): News
  }

  type Mutation {
    create_news(input: NewInput!): News
  }
`;

module.exports = typeDefs;
const { React } = window;
const { setGrid, updateGrid } = window;

class GameGrid extends React.Component {
    constructor() {
        super()
        this.state = {
            gameCellLenght: 15,
            cells: [],
            gameInterval: null,
            gameStart: true,
            generation: 0,
            liveCells: 0
        };

        this.initCells = this.initCells.bind(this);
        this.onCellClicked = this.onCellClicked.bind(this);
        this.checkIfGameCanStart = this.checkIfGameCanStart.bind(this);
        this.startGame = this.startGame.bind(this);
        this.resetGame = this.resetGame.bind(this);
        this.reduceBidimensionalNumberArray = this.reduceBidimensionalNumberArray.bind(this);
    }

    componentWillMount() {
        this.initCells();
    }

    initCells() {
        const gameCellLenght = this.state.gameCellLenght;
        this.setState({
            cells: new Array(gameCellLenght).fill(0).map(() => new Array(gameCellLenght).fill(0))
        });
    }

    onCellClicked(e) {
        const cellIndex = e.target.getAttribute("cellindex").split(",")
        const value = Boolean(Number(e.target.getAttribute("cellvalue")));

        this.state.cells[cellIndex[0]][cellIndex[1]] = Number(!value)
        this.forceUpdate();
    }

    checkIfGameCanStart() {
        const isMatrixAllZeros = this.state.cells.map(row => row.some(cell => cell !== 0));
        isMatrixAllZeros.includes(true) ? this.startGame() : alert("Debe configurar un estado inicial antes de ejecutar el juego.");
    }

    async startGame() {
        const GAME_INTERVAL = 1000
        this.setState({ gameStart: !this.state.gameStart })

        if (!this.state.gameInterval) {
            setGrid(this.state.cells)
            this.setState({
                gameInterval: setInterval(async () => {
                    this.setState({ cells: await updateGrid(), generation: this.state.generation + 1 });
                    this.setState({
                        liveCells: this.reduceBidimensionalNumberArray(this.state.cells)
                    });
                }, GAME_INTERVAL)
            });
        }
        else {
            window.clearInterval(this.state.gameInterval)
            this.setState({ gameInterval: null })
        }
    }

    resetGame() {
        window.clearInterval(this.state.gameInterval)
        this.setState({ gameInterval: null, generation: 0, liveCells: 0 })
        this.initCells();
        alert("Game reseted")
    }

    reduceBidimensionalNumberArray(array) {
        return array.reduce((sum, num) => sum + (Array.isArray(num) ? this.reduceBidimensionalNumberArray(num) : num * 1), 0);
    }

    render() {
        return (
            <React.Fragment>
                <table><tbody>{this.state.cells.map((row, i) => {
                    return <tr key={i}>{row.map((cell, j) => {
                        return <td className={cell === 0 ? "cell" : "cell active"}
                            key={i, j}
                            onClick={this.onCellClicked}
                            cellindex={i.toString() + "," + j.toString()}
                            cellvalue={cell}>
                        </td>
                    })}</tr>
                })}</tbody></table>
                <button onClick={this.checkIfGameCanStart} id="btn-start-game">{this.state.gameStart === true ? "Start" : "Stop"}  Game</button>
                <button style={{ float: "right" }} onClick={this.resetGame} id="btn-start-game">Reset Game</button>

                <div id="stats">
                    <span>Generation: {this.state.generation}</span>
                    <span>
                        Live cells:
                            {this.state.liveCells !== 0
                            ? this.state.liveCells
                            : 0}
                    </span>
                </div>
            </React.Fragment>
        )
    }
}
const { React } = window;
import { render } from "@testing-library/react";
import GameGrid from "./game";

//debería de funcionar pero no
test("Correct size of grid", () => {
    const { getByTestId } = render(<GameGrid />);
    const grid = getByTestId("root");

    expect(grid.style.height).toBe("350px");
    expect(grid.style.width).toBe("350px");
});
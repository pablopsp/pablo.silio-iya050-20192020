const BASE_URL = 'http://localhost:8080/life';

const setGrid = async (gridArray) => {
    return await fetch(BASE_URL, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(gridArray)
    }).then(response => response.json())
        .then(data => { return data });
};


const updateGrid = async () => {
    return await fetch(`${BASE_URL}/next`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        }
    }).then(response => response.json())
        .then(data => { return data });
};

window.setGrid = setGrid;
window.updateGrid = updateGrid;

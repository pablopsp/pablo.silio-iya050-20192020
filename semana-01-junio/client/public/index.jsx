const { React, GameGrid } = window;

class App extends React.Component {
    render() {
        return (
            <GameGrid />
        );
    }
}

ReactDOM.render(<App />, document.getElementById("root"));

describe('Connection to server /hello by GET', () => {
    it('Returns hello world', () => {
        cy.visit('http://localhost:8080/hello')
        cy.request('http://localhost:8080/hello').its('body').should('include', 'Hello World')
    });
});

describe('Connection to server /echo by POST', () => {
    it('Returns the same text that we have sent', () => {
        cy.request('POST', 'http://localhost:8080/echo', { "text": JSON.stringify("Hello from /echo with a POST request") })
            .then((response) => {
                expect(response.status).to.eq(200);
                expect(response.body).to.have.property('text', '"Hello from /echo with a POST request"');
            });
    });
});
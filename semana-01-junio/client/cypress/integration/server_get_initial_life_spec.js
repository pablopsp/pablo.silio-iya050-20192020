describe('Get initial status of life game', () => {
    it('Returns game initial array', () => {
        const expectedResult = [[0,0,0,0,0],[0,0,0,0,0],[0,1,1,1,0],[0,0,0,0,0],[0,0,0,0,0]];
        cy.request('http://localhost:8080/life/').its('body').should('deep.eq', expectedResult)
    });
});
